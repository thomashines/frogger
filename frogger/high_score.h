/*
 * high_score.h
 * 
 * Author: Thomas Hines
 */

#ifndef HIGH_SCORE_H_
#define HIGH_SCORE_H_

#include <stdint.h>

void load_high_scores(void);

void draw_high_scores(void);

uint8_t is_high_score(uint8_t score);

void new_high_score(uint8_t score);

#endif /* HIGH_SCORE_H_ */