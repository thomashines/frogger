/*
 * high_score.c
 *
 * Written by Thomas Hines
 */

#include "high_score.h"

#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <stdio.h>

#include "serialio.h"
#include "terminalio.h"

// Location of high scores in EEPROM
#define START 0

// 8 high scores will be kept
#define KEEP 8

// A name will be 3 chars, or 3 bytes
#define NAME 3

// A score will be an 8 bit integer, or 1 byte
// The position will an 8 bit integer, or 1 byte
// Each high score will be 5 bytes

#define SIZE 5

// The signature to look for at address 0 (8 bits, byte)
#define SIGN (uint8_t)216

char names[KEEP][NAME+1];
uint8_t scores[KEEP];
uint8_t addresses[KEEP];

void load_high_scores(void) {
	// Check signature
	uint8_t sign = eeprom_read_byte((uint8_t*)START);
	if(sign != SIGN) {
		// Initialise eeprom
		// Empty high scores
		for(uint8_t address = 0; address < KEEP; address++) {
			// Position: address
			eeprom_write_byte((uint8_t*)(address*SIZE+START+2), (uint8_t)(address));
			// Name: COM
			eeprom_write_byte((uint8_t*)(address*SIZE+START+3+0), (uint8_t)67);
			eeprom_write_byte((uint8_t*)(address*SIZE+START+3+1), (uint8_t)79);
			eeprom_write_byte((uint8_t*)(address*SIZE+START+3+2), (uint8_t)77);
			if(NAME > 3) {
				for(uint8_t character = 3; character < NAME; character++) {
					eeprom_write_byte((uint8_t*)(address*SIZE+START+3+character), (uint8_t)32);
				}
			}
			// Score: (8 - address)
			eeprom_write_byte((uint8_t*)(address*SIZE+START+3+NAME), (uint8_t)(8-address));
		}
		// Write signature
		eeprom_write_word((uint16_t*)0, SIGN);
	}
	// Load high scores
	for(uint8_t address = 0; address < KEEP; address++) {
		// Position
		uint8_t position = eeprom_read_byte((uint8_t*)(address*SIZE+START+2));
		// Address:
		addresses[position] = address;
		// Name:
		for(uint8_t character = 0; character < NAME; character++) {
			names[position][character] = eeprom_read_byte((uint8_t*)(address*SIZE+START+3+character));
		}
		names[position][NAME] = 0;
		// Score:
		scores[position] = eeprom_read_byte((uint8_t*)(address*SIZE+START+3+NAME));
	}
}

void draw_high_scores(void) {
	move_cursor(10,10);
	printf_P(PSTR("High Scores"));
	for(uint8_t position = 0; position < KEEP; position++) {
		// Name:
		move_cursor(10,12+position);
		printf_P(PSTR("%s"), names[position]);
		// Score:
		move_cursor(16,12+position);
		printf_P(PSTR("%d"), scores[position]);
	}
}

uint8_t is_high_score(uint8_t score) {
	return score >= scores[KEEP-1];
}

void new_high_score(uint8_t score) {
	clear_terminal();
	draw_high_scores();
	move_cursor(10,8);
	printf_P(PSTR("%d: New High Score!"), score);
	move_cursor(10,10);
	printf_P(PSTR("Your Name: ___"));
	move_cursor(21,10);
	// Get name
	// Check for input - which could be a button push or serial input.
	// Serial input may be part of an escape sequence, e.g. ESC [ D
	// is a left cursor key press. At most one of the following three
	// variables will be set to a value other than -1 if input is available.
	// (We don't initalise button to -1 since button_pushed() will return -1
	// if no button pushes are waiting to be returned.)
	// Button pushes take priority over serial input. If there are both then
	// we'll retrieve the serial input the next time through this loop
	uint8_t character = 0;
	char new_name[NAME+1];
	while(character < NAME) {
		uint8_t serial_input = -1;
		// See if there is any serial input
		if(serial_input_available()) {
			// Serial data was available - read the data from standard input
			serial_input = fgetc(stdin);
			// Check if the character is part of an escape sequence
			if(serial_input >= 97 && serial_input <= 122) {
				serial_input -= 32;
			}
			if(serial_input >= 65 && serial_input <= 90) {
				printf_P(PSTR("%c"), serial_input);
				new_name[character] = serial_input;
				character++;
			}
		}
	}
	new_name[NAME] = 0;

	// Find position
	uint8_t new_position = 0;
	uint8_t new_address = 0;
	for(uint8_t position = 0; position < KEEP; position++) {
		if(scores[position] <= score) {
			// This is where the new score fits in
			new_position = position;
			new_address = addresses[KEEP-1];
			break;
		}
	}

	// Increment all positions after this
	for(uint8_t position = KEEP-1; position > new_position; position--) {
		for(character = 0; character < NAME; character++) {
			names[position][character] = names[position-1][character];
		}
		scores[position] = scores[position-1];
		addresses[position] = addresses[position-1];
	}
	
	// Insert new details
	for(character = 0; character < NAME; character++) {
		names[new_position][character] = new_name[character];
	}
	scores[new_position] = score;

	// Save new positions to EEPROM
	addresses[new_position] = new_address;
	for(uint8_t position = 0; position < KEEP; position++) {
		eeprom_write_byte((uint8_t*)(addresses[position]*SIZE+START+2), (uint8_t)(position));
	}
	
	// Save new details
	// Name
	for(character = 0; character < NAME; character++) {
		eeprom_write_byte((uint8_t*)(addresses[new_position]*SIZE+START+3+character), (uint8_t)new_name[character]);
	}
	// Score
	eeprom_write_byte((uint8_t*)(addresses[new_position]*SIZE+START+3+NAME), (uint8_t)score);
}