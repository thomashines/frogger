/*
 * atmel_names.h
 *
 * Author: Thomas Hines
 */ 

// To allow me to use the Atmel Studio names with avr-gcc

#ifndef ATMEL_NAMES_H_
#define ATMEL_NAMES_H_

#define SPIF0 SPIF
#define SPSR0 SPSR
#define SPDR0 SPDR
#define SPR10 SPR1
#define SPR00 SPR0
#define SPI2X0 SPI2X
#define MSTR0 MSTR
#define SPE0 SPE
#define SPCR0 SPCR

#endif /* ATMEL_NAMES_H_ */