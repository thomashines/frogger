/*
 * FroggerProject.c
 *
 * Main file
 *
 * Author: Peter Sutton. Modified by <YOUR NAME HERE>
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>

#include "ledmatrix.h"
#include "scrolling_char_display.h"
#include "buttons.h"
#include "serialio.h"
#include "terminalio.h"
#include "stat.h"
#include "high_score.h"
#include "joystick.h"
#include "timer0.h"
#include "game.h"

#define F_CPU 8000000L
#include <util/delay.h>

// Function prototypes - these are defined below (after main()) in the order
// given here
void initialise_hardware(void);
void splash_screen(void);
void new_game(void);
void play_game(void);
void handle_game_over(void);

// ASCII code for Escape character
#define ESCAPE_CHAR 27

/////////////////////////////// main //////////////////////////////////
int main(void) {
	// Setup hardware and call backs. This will turn on 
	// interrupts.
	initialise_hardware();

	// Load high scores
	load_high_scores();
	
	// Show the splash screen message. Returns when display
	// is complete
	splash_screen();
	
	while(1) {
		new_game();
		play_game();
		handle_game_over();
	}
}

void initialise_hardware(void) {
	ledmatrix_setup();
	init_button_interrupts();
	// Setup serial port for 19200 baud communication with no echo
	// of incoming characters
	init_serial_stdio(19200,0);
	
	init_timer0();
	
	// Turn on global interrupts
	sei();

	// Set C0:4 as outputs
	DDRC = 0x0F;

	// Initialise the joystick
	init_joystick();
}

void splash_screen(void) {
	// Clear terminal screen and output a message
	clear_terminal();
	move_cursor(10,4);
	printf_P(PSTR("Frogger"));
	move_cursor(10,6);
	printf_P(PSTR("CSSE2010/7201 project by Thomas Hines 4317421"));

	// Draw high scores
	draw_high_scores();
	
	// Output the scrolling message to the LED matrix
	// and wait for a push button to be pushed.
	ledmatrix_clear();
	set_text_colour(COLOUR_ORANGE);
	while(1) {
		set_scrolling_display_text("Frogger 43174216");
		// Scroll the message until it has scrolled off the 
		// display or a button is pushed
		while(scroll_display()) {
			_delay_ms(150);
			if(button_pushed() != -1) {
				return;
			}
		}
	}

	// Set random seed
	srand(get_clock_ticks());
}

void new_game(void) {
	// Initialise the game and display
	init_game();
	
	// Clear the serial terminal
	clear_terminal();

	// Draw the vital stats
	init_stats();
	draw_stats();
	
	// Clear a button push or serial input if any are waiting
	// (The cast to void means the return value is ignored.)
	(void)button_pushed();
	clear_serial_input_buffer();
}

void play_game(void) {
	uint32_t current_time;
	uint32_t last_move_time[5];
	int8_t button;
	char serial_input, escape_sequence_char;
	uint8_t characters_into_escape_sequence = 0;
	uint8_t paused = 0;
	
	// Get the current time and remember this as the last time the vehicles
	// and logs were moved.
	current_time = get_clock_ticks();
	for(uint32_t *ptr = last_move_time; ptr < last_move_time + 5; ptr++) {
		*ptr = current_time;
	}

	// We play the game while the frog is alive and we haven't filled up the 
	// far riverbank
	reset_time();
	while(get_lives()) {
		while(is_frog_alive() && (get_time() > 0 || paused)) {
			if(is_frog_alive() && frog_has_reached_riverbank()) {
				// Frog reached the other side successfully but the
				// riverbank isn't full, put a new frog at the start
				add_to_score(1);
				// Increase speed
				if(get_score() < 20) {
					add_to_speed(100 - 5*get_score());
				}
				draw_stats();
				put_frog_at_start();
				// Reset timer
				reset_time();
			}

			if(is_riverbank_full() && get_level() < 4) {
				restore_life();
				next_level();
				draw_stats();
				// Show LED level text
				// Output the scrolling message to the LED matrix
				// and wait for a push button to be pushed.
				ledmatrix_clear();
				clear_scrolling_text();
				char level_text[10];
				sprintf(level_text, "LEVEL %d", get_level());
				set_scrolling_display_text(level_text);
				// Scroll the message until it has scrolled off the 
				// display or a button is pushed
				while(scroll_display()) {
					_delay_ms(50);
					if(button_pushed() != -1) {
						break;
					}
				}
				// Redraw and reinit game
				init_game();
			}
			
			// Check for input - which could be a button push or serial input.
			// Serial input may be part of an escape sequence, e.g. ESC [ D
			// is a left cursor key press. At most one of the following three
			// variables will be set to a value other than -1 if input is available.
			// (We don't initalise button to -1 since button_pushed() will return -1
			// if no button pushes are waiting to be returned.)
			// Button pushes take priority over serial input. If there are both then
			// we'll retrieve the serial input the next time through this loop
			serial_input = -1;
			escape_sequence_char = -1;
			button = button_pushed();
			
			if(button == -1) {
				// No push button was pushed, see if there is any serial input
				if(serial_input_available()) {
					// Serial data was available - read the data from standard input
					serial_input = fgetc(stdin);
					// Check if the character is part of an escape sequence
					if(characters_into_escape_sequence == 0 && serial_input == ESCAPE_CHAR) {
						// We've hit the first character in an escape sequence (escape)
						characters_into_escape_sequence++;
						serial_input = -1; // Don't further process this character
					} else if(characters_into_escape_sequence == 1 && serial_input == '[') {
						// We've hit the second character in an escape sequence
						characters_into_escape_sequence++;
						serial_input = -1; // Don't further process this character
					} else if(characters_into_escape_sequence == 2) {
						// Third (and last) character in the escape sequence
						escape_sequence_char = serial_input;
						serial_input = -1;  // Don't further process this character - we
											// deal with it as part of the escape sequence
						characters_into_escape_sequence = 0;
					} else {
						// Character was not part of an escape sequence (or we received
						// an invalid second character in the sequence). We'll process 
						// the data in the serial_input variable.
						characters_into_escape_sequence = 0;
					}
				}
			}

			// Get joystick input
			uint8_t joystick_state = get_joystick_state();
			
			// Process the input.
			if(!paused) {
				if(button == 3 ||
				   escape_sequence_char == 'D' ||
				   serial_input == 'J' ||
				   serial_input == 'j' ||
				   ((joystick_state & 0b0001) &&
				    !(joystick_state & 0b0010))) {
					// Attempt to move left
					move_frog_left();
				} else if(button == 2 ||
						  escape_sequence_char == 'A' ||
						  serial_input == 'I' ||
						  serial_input == 'i' ||
						  ((joystick_state & 0b0100) &&
						   !(joystick_state & 0b1000))) {
					// Attempt to move forward
					move_frog_forward();
				} else if(button == 1 ||
						  escape_sequence_char == 'B' ||
						  serial_input == 'K' ||
						  serial_input == 'k' ||
						  ((joystick_state & 0b0100) &&
						   (joystick_state & 0b1000))) {
					// Attempt to move down
					move_frog_backward();
				} else if(button == 0 ||
						  escape_sequence_char == 'C' ||
						  serial_input == 'L' ||
						  serial_input == 'l' ||
						  ((joystick_state & 0b0001) &&
						   (joystick_state & 0b0010))) {
					// Attempt to move right
					move_frog_right();
				}
			}
			if(serial_input == 'p' || serial_input == 'P') {
				// Unimplemented feature - pause/unpause the game until 'p' or 'P' is
				// pressed again
				if(paused) {
					unpause_time();
					paused = 0;
				} else {
					pause_time();
					paused = 1;
				}
			} else if(serial_input == 'n' || serial_input == 'N') {
				// Start a new game
				new_game();
			}
			// else - invalid input or we're part way through an escape sequence -
			// do nothing
			
			current_time = get_clock_ticks();
			if(is_frog_alive() && !paused) {
				for(uint8_t i = 0; i < 5; i++) {
					if(current_time > last_move_time[i] + 1200 - 150*i - get_speed()) {
						if(i > 2) {
							scroll_log_channel(i - 3, 1 - 2*(i%2));
						} else {
							scroll_lane(i, 1 - 2*(i%2));
						}
						last_move_time[i] = current_time;
					}
				}
			}
			// Draw the time
			draw_time();
		}
		// We get here if the frog has died or the riverbank is full
		lose_life();
		draw_stats();
		put_frog_at_start();
		reset_time();
	}
	// We get here if the frog is dead and there are no lives left
	// The game is over.
}

void handle_game_over() {
	// Check if new high score
	if(is_high_score(get_score())) {
		new_high_score(get_score());
		clear_terminal();
		draw_stats();
	}
	move_cursor(10,7);
	printf_P(PSTR("GAME OVER"));
	move_cursor(10,8);
	printf_P(PSTR("Press a button to start again"));
	draw_high_scores();
	while(button_pushed() == -1) {
		; // wait
	}
}
