/*
 * stat.h
 * 
 * Author: Peter Sutton
 */

#ifndef STAT_H_
#define STAT_H_

#include <stdint.h>

// All
void init_stats(void);
void draw_stats(void);

// Score
void add_to_score(uint16_t value);
uint16_t get_score(void);

// Lives
void lose_life(void);
void restore_life(void);
uint8_t get_lives(void);

// Level
void next_level(void);
uint8_t get_level(void);

// Speed
void add_to_speed(uint16_t value);
uint16_t get_speed(void);

// Time
void reset_time(void);
void draw_time(void);
uint8_t get_time(void);
void pause_time(void);
void unpause_time(void);

#endif /* STAT_H_ */