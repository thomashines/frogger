/*
 * joystick.h
 *
 * Author: Thomas Hines
 *
 * We assume four push buttons (B0 to B3) are connected to pins B0 to B3. We configure
 * pin change interrupts on these pins.
 */ 


#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include <stdint.h>

void init_joystick(void);

uint8_t get_joystick_state(void);

#endif /* JOYSTICK_H_ */