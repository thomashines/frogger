/*
 * stat.c
 *
 * Written by Peter Sutton
 */

#include "stat.h"

#include <avr/pgmspace.h>
#include <stdio.h>

#include "terminalio.h"
#include "timer0.h"

#define PERIOD 12000

// All
uint8_t lives, level, last_draw_time;
uint16_t score, speed;
uint32_t start_time, paused_time;

void init_stats(void) {
	score = 0;
	lives = 4;
	level = 1;
	speed = 0;
	start_time = 0;
	last_draw_time = 0;
	paused_time = 0;
}

void draw_stats(void) {
	// Serial
	move_cursor(10,4);
	printf_P(PSTR("Score: %d"), score);
	move_cursor(10,5);
	printf_P(PSTR("Lives: %d"), lives);
	move_cursor(20,4);
	printf_P(PSTR("Level: %d"), level);
	move_cursor(20,5);
	printf_P(PSTR("Speed: %d"), speed);

	// Lives LED
	PINC &= 0xF0;
	PINC |= lives;
}

// Score
void add_to_score(uint16_t value) {
	score += value;
}

uint16_t get_score(void) {
	return score;
}

// Lives
void lose_life(void) {
	lives--;
}

void restore_life(void) {
	if(lives < 4) {
		lives++;
	}
}

uint8_t get_lives(void) {
	return lives;
}

// Level
void next_level(void) {
	level++;
}

uint8_t get_level(void) {
	return level;
}

// Speed
void add_to_speed(uint16_t value) {
	speed += value;
}

uint16_t get_speed(void) {
	return speed;
}

// Time
void reset_time(void) {
	start_time = get_clock_ticks();
	last_draw_time = PERIOD/1000 + 1;
	draw_time();
}

void draw_time(void) {
	if(!paused_time) {
		if((PERIOD - get_clock_ticks() + start_time)/1000 != last_draw_time) {
			last_draw_time = (PERIOD - get_clock_ticks() + start_time)/1000;
			move_cursor(10,14);
			printf_P(PSTR("Time: %02d"), last_draw_time);
		}
	}
}

uint8_t get_time(void) {
	if(get_clock_ticks() - start_time > PERIOD) {
		return 0;
	} else {
		return (PERIOD - get_clock_ticks() + start_time)/1000;
	}
}

void pause_time(void) {
	paused_time = get_clock_ticks();
}

void unpause_time(void) {
	start_time += get_clock_ticks() - paused_time;
	paused_time = 0;
}