/*
 * joystick.c
 *
 * Author: Thomas Hines
 */ 

#include <avr/io.h>
#include "joystick.h"

uint8_t last_state;
uint16_t held_cycles;

void init_joystick(void) {
	// Input selection doesn't matter yet - we'll swap this around in the while
	// loop below.
	ADMUX = (1<<REFS0);
	// Turn on the ADC (but don't start a conversion yet). Choose a clock
	// divider of 64. (The ADC clock must be somewhere
	// between 50kHz and 200kHz. We will divide our 8MHz clock by 64
	// to give us 125kHz.)
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(0<<ADPS0);
	// Clear last state
	last_state = 0;
	// Clear held cycles
	held_cycles = 0;
}

uint8_t get_joystick_state(void) {
	// Read joystick direction
	// bits
	// 0: X pressed
	// 1: X direction (0: left, 1:right)
	// 2: Y pressed
	// 3: Y direction (0: up, 1:down)
	// Read joystick
	// X
	ADMUX &= ~1;
	// Start the ADC conversion
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC)) {
		; // Wait until conversion finished
	}
	uint16_t x_value = ADC; // read the value
	// Y
	ADMUX |= 1;
	// Start the ADC conversion
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC)) {
		; // Wait until conversion finished
	}
	uint16_t y_value = ADC; // read the value
	// Determine direction: values between 0 and 1024
	uint8_t ret = 0;
	// X
	if(x_value < 256) {
		// X is left
		ret |= (1 << 0) | (0 << 1);
	} else if(x_value > 768) {
		// X is right
		ret |= (1 << 0) | (1 << 1);
	}
	// Y
	if(y_value > 768) {
		// Y is up
		ret |= (1 << 2) | (0 << 3);
	} else if(y_value < 256) {
		// Y is down
		ret |= (1 << 2) | (1 << 3);
	}
	if(ret == last_state && held_cycles < (1 << 8)) {
		held_cycles++;
		return 0;
	} else {
		last_state = ret;
		held_cycles = 0;
		return ret;
	}
}